#!/bin/bash

# script pour compiler le rapport (sans erreurs)

if [ `dpkg -l | grep biber | wc -l` -eq 0 ]
then
	sudo apt install -y biber
fi

echo First compile:
pdflatex rapport.tex | grep Warning
echo Biber compile:
biber rapport | grep Warning
echo Second compile:
pdflatex rapport.tex | grep Warning
echo Third compile:
pdflatex rapport.tex | grep Warning