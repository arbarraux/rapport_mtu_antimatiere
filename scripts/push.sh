number=`cat README.md | grep artifacts | cut -f 8 -d /`
number=`expr $number + 2`
sed -i "5d" README.md
echo "https://gitlab.isima.fr/arbarraux/rapport_mtu_antimatiere/-/jobs/"$number"/artifacts/file/public/rapport.pdf" >> README.md

echo Message du commit:
read commit_message
git add *
git commit -m "$commit_message"
git push